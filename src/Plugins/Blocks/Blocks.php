<?php

namespace Creativehandles\ChBlocks\Plugins\Blocks;

use App\Helpers\Slug;
use Creativehandles\ChBlocks\Plugins\Plugin;
use Creativehandles\ChBlocks\Plugins\Blocks\Models\Block;
use Creativehandles\ChBlocks\Plugins\Blocks\Models\Folder;
use Creativehandles\ChBlocks\Plugins\Blocks\Models\Overfolder;
use App\Helpers\Plugins;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;

class Blocks extends Plugin
{


  public function GetFolders()
  {
    if (Plugins::isActive("Groups")) {
      return Folder::orderBy("position", 'ASC')->get();
    } else {
      return Folder::all();
    }

  }

  public function GetErasedFolders()
  {
    return Folder::onlyTrashed()->get();
  }

  public function GetErasedOverFolders()
  {
    return Overfolder::onlyTrashed()->get();
  }

  public function GetOverFolders()
  {
    return Overfolder::all();
  }


  public function GetBlocks()
  {
    return Block::all();
  }

  public function createFolder(string $formLocale, string $name)
  {
    $folder = new Overfolder();
    $folder->system_name = Slug::create($name);

    $folderTranslation = $folder->translateOrNew($formLocale);
    $folderTranslation->folder = $name;

    return $folder->save();
  }

  public function createSubFolder(string $formLocale, String $name, Int $id)
  {
    $folder = new Folder();
    $folder->overfolder = $id;
    $folder->system_name = Slug::create($name);

    $folderTranslation = $folder->translateOrNew($formLocale);
    $folderTranslation->folder = $name;

    return $folder->save();
  }

  public function getSubfoldersOverFolder(Int $id)
  {
    $folder = Folder::find($id);
    $overFolder = Overfolder::find($folder->overfolder);

    return $overFolder;
  }

  public function getFolder(Int $id)
  {
    return Folder::find($id);
  }

  public function getOverfolder(Int $id)
  {
    return Overfolder::find($id);
  }

    /**
     * Get a Block instance with the translation specified by locale.
     *
     * @param int $id ID of the Block
     * @param string $locale Locale of the translation
     * @return Model|null
     */
  public function getBlock(int $id, string $locale = null)
  {
      $block = Block::find($id);

      if ($block === null) {
          return null;
      }

      if ($locale !== null) {
          $block->setDefaultLocale($locale);
      }

      return $block;
  }

  public function getBlocksInFolder(Int $id)
  {
    return Block::where('folder', $id)->get();
  }

  public function getOrderedBlocksInFolder(Int $id, String $direction)
  {
    return Block::where('folder', $id)->orderBy('position', $direction)->get();
  }

  public function getOrderedFoldersInFolder(Int $id, String $direction)
  {
    return Folder::where('overfolder', $id)->orderBy('position', $direction)->get();
  }

  /**
   * Save a block into DB - if blocks exists, its only update
   * the record
   *
   * @param $data
   * @return bool
   */
  public function save($data): bool
  {
    if (array_key_exists('blockID', $data)) {
      // search block in DB
      $block = Block::find($data['blockID']);
    } else {
      $block = new Block();
    }

    $blockTranslation = $block->translateOrNew($data['form_locale']);
    $blockTranslation->name = $data['title'];
    $blockTranslation->content = $data['html'];
    $block->folder = $data['folderID'];
    $blockTranslation->image = $data['image'] ?? null;

    $block->save();

    if (!array_key_exists('blockID', $data)) {
      $block->system_name = 'block-' . $block->id;
      $block->save();
    }

    return true;

  }

  /**
   * Remove block with given ID
   *
   * @param Int $id
   * @return bool
   */
  public function removeBlock(Int $id): bool
  {
    $block = Block::find($id);
    if ($block->delete()) {
      return true;
    }

    return false;
  }


  /**
   * Save position of blocks if you use drag&drop function
   *
   * @param array $array
   * @return bool
   */
  public function savePosition(Array $array): bool
  {
    foreach ($array as $item) {
      $block = Block::find($item['id']);
      $block->position = $item['position'];
      $block->save();
    }

    return true;
  }

    /**
     * Rename an overfolder by given id and new name
     *
     * @param int $id
     * @param string $formLocale
     * @param string $folderName
     * @return bool
     */
    public function renameOverfolder(int $id, string $formLocale, string $name): bool
    {
        $folder = Overfolder::find($id);

        $folderTranslation = $folder->translateOrNew($formLocale);
        $folderTranslation->folder = $name;

        return $folder->save();
    }

    /**
     * Renames folder by given id and new name
     *
     * @param Int $id
     * @param string $formLocale
     * @param String $folderName
     * @return bool
     */
  public function renameFolder(Int $id, string $formLocale, String $name): bool
  {
    $folder = Folder::find($id);

    $folderTranslation = $folder->translateOrNew($formLocale);
    $folderTranslation->folder = $name;

    return $folder->save();
  }

  public function removeFolder(Int $folderID)
  {
    $folder = Overfolder::find($folderID);
    if ($folder) {

      // delete folder itself
      $folder->delete();

      // delete all blocks in this folder
      // TODO: Add correct recursive delete of all subfolders and blocks in them
      //Block::where('folder', $folder->id)->delete();

      return true;
    }
    return false;
  }

  public function removeSubFolder(Int $folderID)
  {
    $folder = Folder::find($folderID);
    if ($folder) {

      // delete folder itself
      $folder->delete();

      // delete all blocks in this folder
      Block::where('folder', $folder->id)->delete();

      return true;
    }
    return false;
  }

  public function restoreFolder($folderID)
  {
    $folder = Folder::onlyTrashed()->where('id', $folderID)->restore();
    $blocks = Block::onlyTrashed()->where('folder', $folderID)->restore();

    return true;
  }

  public function restoreOverFolder($folderID)
  {
    $overFolder = Overfolder::onlyTrashed()->where('id', $folderID)->restore();
    // TODO: Correctly restore all related subfolders and blocks
//    $folder = Folder::onlyTrashed()->where('id', $overFolder->id)->restore();
//    $blocks = Block::onlyTrashed()->where('folder', $folder->id)->restore();

    return true;
  }


  /**
   * Helper classes
   *
   * Interact with templates easily. All methods are static!
   */


  /**
   * Return back all blocks in folder
   *
   * @param String $folderSystemName
   * @return mixed
   */
  public static function getF(String $folderSystemName)
  {
    if (Plugins::isActive('Blocks')) {
      $folder = Folder::where('system_name', $folderSystemName)->first();
      if ($folder) {
        $blocks = Block::where('folder', $folder->id)->get();

        return $blocks;
      }

      return false;

    } else {
      return false;
    }
  }


  public static function getB(String $blockSystemName, String $column)
  {
    if (Plugins::isActive('Blocks')) {

      $block = Block::where('system_name', $blockSystemName)->first();
      if ($block) {
        return $block->$column;
      }
    } else {
      return false;
    }

  }

  /**
   * Duplicates a block
   *
   * @param int $id
   * @param string $locale Locale of the translation
   * @return bool
   */
  public function duplicateBlock(int $id, string $locale = null)
  {
    $block = Block::find($id);

    if ($block) {
      $newBlock = $block->replicate();
      $newBlock->save();

      if ($locale !== null) {
          $block->setDefaultLocale($locale);
          $newBlock->setDefaultLocale($locale);
      }

      $newBlock->system_name = 'block-' . $newBlock->id;
      $newBlock->name = $block->name . ' - duplikát';

      return $newBlock->save() ? true : false;
    }

    return false;
  }

  /*
   * Get all subfolders in folder
   */
  public static function getAllSubfolders(Int $id)
  {
    $folders = Folder::where('overfolder', $id)->orderBy("position", "ASC")->get();
    if($folders) {
      return $folders;
    }

    return false;
  }

}
