<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropTranslationAttrsFromBlocksFolderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // we drop the translation attributes in our main table:
        Schema::table('blocks_folder', function ($table) {
            $table->dropColumn('folder');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // add the translatable attributes back in to main table
        Schema::table('blocks_folder', function ($table) {
            $table->string('folder', 200)->default(null)->nullable()->after('id');
        });
    }
}
