<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlocksFolderTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blocks_folder_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('blocks_folder_id');
            $table->char('locale', 2)->index();
            $table->string('folder', 200)->default(null)->nullable();
            $table->timestamps();

            $table->unique(['blocks_folder_id', 'locale'], 'blocks_folder_id_locale');
            $table->foreign('blocks_folder_id', 'fk_blocks_folder_id')->references('id')->on('blocks_folder')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blocks_folder_translations');
    }
}
