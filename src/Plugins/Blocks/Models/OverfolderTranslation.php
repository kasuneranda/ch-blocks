<?php

namespace Creativehandles\ChBlocks\Plugins\Blocks\Models;

use Illuminate\Database\Eloquent\Model;

class OverfolderTranslation extends Model
{
    protected $table = 'overfolder_translations';
    public $fillable = ['folder'];
}
