<?php

namespace Creativehandles\ChBlocks\Plugins\Blocks\Models;

use Illuminate\Database\Eloquent\Model;

class BlockTranslation extends Model
{
    protected $table = 'block_translations';
    public $fillable = ['name', 'content', 'image', 'textarea'];
}
