<?php

namespace Creativehandles\ChBlocks\Plugins\Groups;
use Creativehandles\ChBlocks\Plugins\Plugin;
use Creativehandles\ChBlocks\Plugins\Groups\Models\Group;
use Creativehandles\ChBlocks\Plugins\Blocks\Models\Folder;
use App\Helpers\Plugins;

class Groups extends Plugin {


  public function getRandomColor()
  {
    return sprintf('#%06X', mt_rand(0, 0xFFFFFF));
  }

  public function createGroup(String $name)
  {
    $group = new Group();
    $group->name = $name;
    $group->color = $this->getRandomColor();
    if($group->save()) {
      return true;
    }
    return false;
  }

  public function assignGroup($groupID, Int $folderID)
  {
    $folder = Folder::find($folderID);
    $folder->group = $groupID;
    if($folder->save()) {
      return true;
    }
    return false;
  }

  public static function getGroups()
  {
    $groups = Group::all();

    return $groups ? $groups : false;
  }

  public static function getGroup($folderID)
  {
    $folder = Folder::find($folderID);
    $group = Group::find($folder->group);

    return $group ? $group : false;
  }

  public static function getGroupByID($groupID)
  {
    $group = Group::find($groupID);

    return $group ? $group : false;
  }


  /**
   * Save position of folders if you use drag&drop function
   *
   * @param array $array
   * @return bool
   */
  public function savePosition(Array $array) : bool
  {
    foreach($array as $item) {
      $folder = Folder::find($item['id']);
      $folder->position = $item['position'];
      $folder->save();
    }

    return true;
  }

  /**
   * Return back all blocks in folder
   *
   * @param String $folderSystemName
   * @return mixed
   */
  public static function getAllFoldersInGroup(Int $groupID)
  {
    if(Plugins::isActive('Blocks') AND Plugins::isActive('Groups')) {
      $folders = Folder::where('group', $groupID)->orderBy("position", "ASC")->get();
      if($folders) {
        return $folders;
      }

      return false;

    } else {
      return false;
    }
  }

}