<?php

namespace Creativehandles\ChBlocks\Plugins\Groups\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Group extends Model
{
    protected $table = 'groups';
}
